import numpy as np

np.random.seed(0)

num_rows = 20
num_cols = 3


def gen_data(nrows, ndims, a, b):
  X = np.random.randn(nrows, ndims)
  Y = a + np.dot(X,b.transpose())
  return(X,Y)

# Should be scaled before adding the bias term to first column
def scale_data(X):
  stat_array = []
  for i in range(num_cols):
    col = X[:,i]
    Xmin = min(col)
    Xmax = max(col)
    X[:,i] = (X[:,i] - Xmin) / (Xmax - Xmin)
    stat_array.append([Xmin,Xmax])

  return(stat_array)

def gen_data_wrapper():
  a_init = 2.0
  b_init = np.array([5.0, 3.0, 2.0])
  X,Y = gen_data(num_rows, num_cols, a_init, b_init)
  return(X,Y)


def get_Ypred(weights):
  #print(weights)
  Ypred = np.dot(X,weights.transpose())
  return(Ypred)

def get_E(weights):
  delta = 1e-6
  y1 = get_Ypred(weights)
  e1 = np.dot(Y - y1, Y - y1)
  print("Y1 ",y1)
  weights[0] += delta
  y2 = get_Ypred(weights)
  e2 = np.dot(Y - y2, Y - y2)
  print("Y2 ",y2)
  de_dw0 = (e2 - e1) / delta
  weights[0] -= delta
  print("Finite diff error gradient",de_dw0)

  computed_de_dw = deltaF(weights)
  print("From subroutine ",computed_de_dw[0])

def deltaF(weights):
  Ypred = get_Ypred(weights)
  dE_dw = -2.0 * np.dot((Y - Ypred).transpose(), X)
  return(dE_dw)


def batch_get_Ypred(weights,X,Y):
  #print(weights)
  Ypred = np.dot(X,weights.transpose())
  return(Ypred)


def batch_deltaF(weights,X,Y):
  Ypred = batch_get_Ypred(weights,X,Y)
  dE_dw = -2.0 * np.dot((Y - Ypred).transpose(), X)
  return(dE_dw)

X,Y = gen_data_wrapper()
X_mod = np.zeros([num_rows, num_cols+1])
X_mod[:,1:] = X
X_mod[:,0] = np.ones(num_rows).transpose()
X = X_mod



def grad_descent():
  global X,Y
  weights = np.array([2.5,4.0, 3.5, 2.5])
  tol = 1e-3
  max_iterations = 1000
  step_size = 0.001

  iterations = 0
  tolerance = 100

  while((tolerance > tol) and (iterations < max_iterations)):
    update = step_size * deltaF(weights)
    weights -= update
    iterations += 1
    tolerance = (abs(update)).mean()
    print(" iteration %d  -- %f " %(iterations,tolerance))

  print("Computed weights ", weights)


def batch_grad_descent():
  global X,Y,num_batches
  weights = np.array([2.5,4.0, 3.5, 2.5])
  tol = 1e-3
  max_iterations = 100
  step_size = 0.001

  iterations = 0
  tolerance = 100
  n_batches = 4
  batch_size = int(num_rows / n_batches)
  print(batch_size)

  while((tolerance > tol) and (iterations < max_iterations)):
    for i in range(n_batches):
        Xbatch = X[i*batch_size : (i+1)*batch_size , :]
        Ybatch = Y[i*batch_size : (i+1)*batch_size]
        update = step_size * batch_deltaF(weights,Xbatch,Ybatch)
        weights -= update
        tolerance = (abs(update)).mean()
        print(" iteration %d batch %d -- %f " %(iterations,i,tolerance))
    iterations += 1
  print("Computed weights ", weights)


grad_descent()
batch_grad_descent()
