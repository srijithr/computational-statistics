import numpy as np
import sklearn as sk
from sklearn import datasets

batch_size = 4

RUN_TILL_END_RESTART = False
RUN_TILL_END_AND_STOP = False
WRAP_AROUND = True

def get_data():

    dataset = datasets.load_iris()
    data, target = dataset.data, dataset.target
    return(data, target)


def get_batch(data, target):

    curr = 0
    total_size = len(target)
    while( curr < total_size ):
        start = curr
        end = (curr + batch_size)%total_size
        if(start == 148):
            print("In conditional breakpoint")
        print("Current ",start,end)
        if((end < start) and WRAP_AROUND == True):
            size_to_end = total_size - curr
            size_leftover = batch_size - size_to_end
            data_chunk_to_end = data[curr:]
            target_chunk_to_end = target[curr:]
            data_chunk_leftover = data[0:end]
            target_chunk_leftover = target[0:end]
            data_return = np.vstack((data_chunk_to_end, data_chunk_leftover))
            target_return = np.append(target_chunk_to_end, target_chunk_leftover)
            yield(data_return, target_return)
        elif((end < start) and ((RUN_TILL_END_RESTART == True) or (RUN_TILL_END_AND_STOP == True))):
            yield(data[curr:], target[curr:])
        else:
            yield([data[start: end], target[start: end]])


        if (RUN_TILL_END_RESTART == True):
                curr = (curr + batch_size)
                if(curr > total_size):
                    curr = 0
        if(RUN_TILL_END_AND_STOP == True):
                curr = curr + batch_size
        if(WRAP_AROUND == True):
                curr = (curr + batch_size)%total_size

data, target = get_data()
batch_generator = get_batch(data, target)

for i in range(0,40):

    batch = next(batch_generator)
    batch_data = batch[0]
    batch_target = batch[1]
    print("------------ Iteration %d ---------- "%(i))
    print(batch_data)
    print(batch_target)
