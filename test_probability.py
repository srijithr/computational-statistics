import numpy as np
import plotly
import plotly.graph_objs as go

def plot_object(object):
    plotly.offline.plot({
    "data": [go.Scatter(x=object.model['x'], y=object.model['values'])],
    "layout": go.Layout(title="Plot",yaxis=range(-3,3))
}, auto_open=True)


def plot(**kwargs):

    yval = kwargs['yval']
    if('xval') in kwargs.keys():
        xval = kwargs['xval']
    else:
        xval = np.arange(0,len(yval))

    layout = go.Layout(
    yaxis=dict(
        range=[2, 5]
    )
    )
    plotly.offline.plot({
    "data": [go.Scatter(x=xval, y=yval)],
    #"layout": layout
    }, auto_open=True)



class Gaussian():

    def __init__(self,params):
        self.model = {}
        self.model['mean'] = params['mean']
        self.model['sigma'] = params['sigma']
        self.model['resolution'] = params['resolution']
        self.model['x'], self.model['values'] = self.get_gaussian()

    def get_gaussian(self):
        mu = self.model['mean']
        sigma = self.model['sigma']
        n = self.model['resolution']
        x = np.linspace(mu -sigma*5,mu + sigma*5,n)
        val = np.exp( -(x - mu)**2 / (2.0*(sigma**2)) ) / np.sqrt(2.0*np.pi*(sigma**2))
        return(x,val)

    def sample_gaussian(self,obj,n):
        b = np.random.normal(obj['mean'],obj['sigma'],n)
        return(b)

    def get_sample(self,obj,n):
        randx = np.random.choice(obj['resolution'],n,list(obj['values']))
        randy = obj['values'][randx]
        return(randy)

    def get_gaussian_mle(self,samples):
        mean = samples.sum() / len(samples)
        sigma = ((samples - mean)**2).sum() / len(samples)
        return(mean,np.sqrt(sigma))

    def plot(self):
        plot_object(self)



class RBF():

    def __init__(self,params):
        self.model = {}
        self.model['sigma'] = params['sigma']
        self.model['l'] = params['l']
        self.model['resolution'] = params['resolution']


    def get_rbf(self,x,Xi,Xj):
        dim = np.shape(Xi)[1]
        term = np.sum(Xi**2,1).reshape(-1,dim) + np.sum(Xj**2,1) - 2*np.dot(Xi, Xj.T)
        self.model['values'] = self.model['sigma']**2 * np.exp(-0.5 * term / (self.model['l']**2))
        self.model['x'] = x

    def plot(self):
        plot_object(self)


class Probability():

    def __init__(self,a,b,res):
        self.a = a
        self.b = b
        self.resolution = res

    def get_posterior_bayes(self,class_likelihood,prior,p_x_c0):

        p_c1 = prior
        p_c0 = 1 - p_c1
        p_x_c1 = class_likelihood

        evidence = p_x_c0 * p_c0 + p_x_c1 * p_c1
        # prior is class unconditional class probability P(Ci)
        # posterior is conditional probability P(C1_x) of the class C1 if x happened
        # class_likelihood is conditional probability P(x_C1) of x if C1 is the class
        posterior = class_likelihood * prior / evidence
        return(posterior*100.0)



def bayes_test():
    # Test bayes theorem
    prior = 0.004
    class_likelihood = 0.8
    p_x_c0 = 0.1
    return(t.get_posterior_bayes(class_likelihood,prior,p_x_c0))


def mle_test():
    params = {}
    params['mean'] = 0
    params['sigma'] = 1
    params['resolution'] = 1000
    gaussian_model = Gaussian(params)
    samples = gaussian_model.sample_gaussian(gaussian_model.model,20)
    mle_mean, mle_sigma = gaussian_model.get_gaussian_mle(samples)

    mle_params = {}
    mle_params['mean'] = mle_mean
    mle_params['sigma'] = mle_sigma
    mle_params['resolution'] = params['resolution']
    mle_gaussian = Models('gaussian',mle_params)
    return(gaussian_model.model['x'],gaussian_model.model['values'],samples,
           mle_gaussian.model['x'], mle_gaussian.model['values'],mle_mean,mle_sigma)

def test_rbf():
    params = {}
    params['l'] = 0.1
    params['sigma'] = 0.005
    params['resolution'] = 1000
    rbf_model = RBF(params)
    x = np.arange(-5,5,0.1).reshape(-1,1)
    rbf_model.get_rbf(x,x,x)
    K = rbf_model.model['values']
    L = np.linalg.cholesky(K + 1e-15*np.eye(len(x)))
    print(x.T[0])
    fprior = np.dot(L,np.random.normal(size=(len(x),1))).T
    print(fprior)
    plot(xval=x.T[0],yval=fprior[0])




test_rbf()
